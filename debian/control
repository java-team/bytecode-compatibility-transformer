Source: bytecode-compatibility-transformer
Section: java
Priority: optional
Maintainer: Debian Java Maintainers <pkg-java-maintainers@lists.alioth.debian.org>
Uploaders: Emmanuel Bourg <ebourg@apache.org>
Build-Depends: cdbs,
               debhelper (>= 9),
               default-jdk,
               ivy,
               junit4,
               libannotation-indexer-java,
               libasm4-java,
               libcommons-io-java,
               libjenkins-constant-pool-scanner-java,
               libmaven-antrun-plugin-java,
               libmaven-dependency-plugin-java,
               maven-debian-helper (>= 1.5)
Standards-Version: 3.9.6
Vcs-Git: git://anonscm.debian.org/pkg-java/bytecode-compatibility-transformer.git
Vcs-Browser: http://anonscm.debian.org/gitweb/?p=pkg-java/bytecode-compatibility-transformer.git
Homepage: https://github.com/jenkinsci/bytecode-compatibility-transformer

Package: libbytecode-compatibility-transformer-java
Architecture: all
Depends: ${maven:Depends}, ${misc:Depends}
Suggests: ${maven:OptionalDepends}
Description: Bytecode transformation-based library for managing backward compatibility
 This Java library provides a set of annotations and bytecode transformer
 that helps evolving a modular codebase without losing compatibility. Field
 access can be adapted to either getter/setter methods or to another field.
 Both instance and static fields can be adapted. The type of a field can also
 be changed to a subtype.
